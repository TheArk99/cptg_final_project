//headers and c++ std lib
#include <iostream>
#include <getopt.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <cstring>
#include <iomanip>
#include <fstream>
#include <filesystem>
//in case of any need for multithreading, for posix systems only:
//#include <pthread.h>
//if wanting to use vectors instead of struct or classes
//#include <vector>
//using std::vector;
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::getline;

//custom defined headers
#include "headers/functions.h"


int main(void){
  const string filePath = "addressBook.txt";
  string searchQueryFirstName;
  string indexString;
  string yN;
  bool exit = false;
  bool canDelete;
  bool goodFileContents = false;
  int userChoice = NULL;
  int indexInt;
  int arrSize;
  if (checkFileExistence(filePath) && getFileLineCout(filePath) > 0){
    arrSize = getFileLineCout(filePath);
    canDelete = true;
    goodFileContents = true;
  }else{
    arrSize = 1;
    canDelete = false;
  }

  //malloc for some reason gives seg faults but new does not???
 // People* person = (People*)malloc( arrSize * sizeof(People));
  //calloc for contigous allocation maybe?
  People* person = (People*)calloc(arrSize, sizeof(People));
  //new doesn't seem to work with realloc
//  People* person = new People[arrSize];
//  vector<People> person(arrSize);
//  People person[100];
  if (person == nullptr){
    std::cerr << "person struct array not allocated correctly" << endl;
  }


    // apparently need to inizalize members of struct after allocating mem
    for (int i = 0; i < arrSize; ++i) {
        new (&person[i].firstName) string();
        new (&person[i].lastName) string();
        new (&person[i].phoneNum) string();
        new (&person[i].email) string();
        new (&person[i].city) string();
        new (&person[i].state) string();
        new (&person[i].address) string();
        new (&person[i].zip) string();
        new (&person[i].age) string();
    }

  if (goodFileContents == true){
    std::ifstream file(filePath);
    if (!file.is_open()) {
        std::cerr << "Error opening the file." << endl;
        return 1;
    }
    int count = 0;
    //loops to put file contents into each elements member in the struct arr
    while (count < arrSize &&
        file >> person[count].firstName >> person[count].lastName >> person[count].phoneNum
        >> person[count].age >> person[count].email >> person[count].city
        >> person[count].state >> person[count].address >> person[count].zip){
        count++;
    }
    file.close();
  }

  while (exit == false){
    system("clear"); //bash call to clear screen
    if (userChoice == NULL || userChoice > 9 || userChoice < 1){ //checking if user enters wrong info
      userChoice = menu();
      if (userChoice == NULL || userChoice > 9 || userChoice < 1){ // if they did again exits with stderr and return status of -1
        fprintf(stderr, "ERORR, DID NOT USE CORRECT OPTIONS...EXITING\n");
        return -1;
      }
    }
    switch(userChoice){
      case 1://add contact
        system("clear");
        if (person[0].firstName == ""){ //checks if there is anything filled out
          addPeople(person, arrSize - 1);
          break;
        }
        arrSize++;
        //re-allocating mem to fit size after new elements are added
        person = (People*)realloc(person, arrSize * sizeof(People));
//        person.resize(arrsize);//for the vector use but i am not using that now so who cares
        //checking if allocated correctly
        if (person == nullptr){
          //I found out the C++ way to do stderr, but idk if i care because i like C better so who cares
          std::cerr << "person struct array not allocated correctly" << endl;
        }
        //like before need to inizalize members of struct to C++ string
        new (&person[arrSize - 1].firstName) string();
        new (&person[arrSize - 1].lastName) string();
        new (&person[arrSize - 1].phoneNum) string();
        new (&person[arrSize - 1].email) string();
        new (&person[arrSize - 1].city) string();
        new (&person[arrSize - 1].state) string();
        new (&person[arrSize - 1].address) string();
        new (&person[arrSize - 1].zip) string();
        new (&person[arrSize - 1].age) string();
        //calls func to add person to the struct arr
        addPeople(person, arrSize - 1);
        break;
      case 2://search for contact
        if (person[0].firstName == ""){
          cout << "Need to have more than 0 contacts added before you can search" << endl;
          break;
        }
        system("clear");
        cout << "Enter first name of person to search for: ";
        getline(cin, searchQueryFirstName);
        int index;
        searchPeople(person, arrSize, searchQueryFirstName, index);
        if (index == -1){
          std::cerr << "NO name found" << endl;
          break;
        }
        cout << "Found index of person at: " << index << endl;
        cout << endl << "Would you like to print out contents of element? [y/n]: ";
        getline(cin, yN);
        if (yN == "y" || yN == "Y"){
          printStruct(person, index);
        }
        break;
      case 3://print all contacts
        if (person[0].firstName == ""){
          cout << "Need to have more than 0 contacts added before you can print" << endl;
          break;
        }
        system("clear");
        for (int i = 0; i < arrSize; i++){
          printStruct(person, i);
        }
        break;
      case 4://print 1 specific contact specified by user
        if (person[0].firstName == ""){
          cout << "Need to have more than 0 contacts added before you can print" << endl;
          break;
        }
        system("clear");
        cout << "Enter first name of contact: ";
        getline(cin, indexString);
        searchPeople(person, arrSize, indexString, indexInt);
        printStruct(person, indexInt);
        break;
      case 5://edit contact
        if (person[0].firstName == ""){
          cout << "Need to have more than 0 contacts added before you can edit" << endl;
          break;
        }
        system("clear");
        cout << "Enter first name of person to search for: ";
        getline(cin, searchQueryFirstName);
        int whichIndexToChange;
        searchPeople(person, arrSize, searchQueryFirstName, whichIndexToChange);
        if (whichIndexToChange == -1){
          std::cerr << "NO name found" << endl;
          break;
        }
        changePeople(person, whichIndexToChange);
        break;
      case 6://delete contact
        if (canDelete == false && person[0].firstName == ""){
          std::cerr << "Have no elements, can not delete" << endl;
          break;
        }
        system("clear");
        cout << "Enter first name of person to search for: ";
        getline(cin, searchQueryFirstName);
        int whichIndexToDel;
        searchPeople(person, arrSize, searchQueryFirstName, whichIndexToDel);
        if (whichIndexToDel == -1){
          std::cerr << "NO name found" << endl;
          break;
        }
        cout << endl << "Are you sure you want to delete the contact for: "
          << person[whichIndexToDel].firstName << " "
          << person[whichIndexToDel].lastName <<
          "? [y/n]: ";
        getline(cin, yN);
        if (yN == "y" || yN == "Y"){
          if (arrSize > 1 ){
            arrSize--;
          }
          delContact(person, whichIndexToDel, arrSize);
        }
        break;
      case 7://sort all contacts
        if (person[0].firstName == ""){
          cout << "Need to have more than 0 contacts added before you can sort" << endl;
          break;
        }
        sortArr(person, arrSize);
        cout << "Sorted list";
        break;
      case 8://save changes to file located in users home dir in cache dir under name "~/.cache/addressBook.txt"
        cout << "Saving..." << endl;
        saveStructToFile(person, arrSize, filePath);
        break;
      case 9://Exit
        cout << "Exiting...\n";
        exit = true;
        continue;
    }
    hitEnter();//pauses until given input allowing user to view content used and or printed to stdout (also looked nice from dr. hwang's examples)
    userChoice = NULL; // reset var to avoid infinite looping
  }
  free(person); //end of program deallocate struct arr person
  return 0;
}
