#c compile flags
CC := g++
FLAGS := -Wall -std=c++20 -g -o
OPTIMIZE2 := -O2
OPTIMIZE3 := -O3

#calling all
all: main

#main file/script
main: main.cpp
	$(CC) $(FLAGS) bin/main main.cpp headers/functions.cpp

optimize2: main.cpp
	$(CC) $(OPTIMIZE2) $(FLAGS) bin/main main.cpp headers/functions.cpp

optimize3: main.cpp
	$(CC) $(OPTIMIZE3) $(FLAGS) bin/main main.cpp headers/functions.cpp

clean:
	rm -rf bin addressBook.txt

install: clean
	mkdir bin/
	$(CC) $(OPTIMIZE3) $(FLAGS) bin/main main.cpp headers/functions.cpp
