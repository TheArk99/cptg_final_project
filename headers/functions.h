#ifndef _FUNCS_
#define _FUNCS_
#include <string>
#include <iostream>
using std::string;

//people
typedef struct People{
  string firstName;
  string lastName;
  string phoneNum;
  string email;
  string city;
  string state;
  string address;
  string zip;
  string age;
};


int menu(void);
//void sortArr(std::string *unsortedArr, int arrSize);
void sortArr(People unsortedArr[], int arrSize);
//void swap(int &x, int &y);
void swap(People &x, People &y);
void hitEnter(void);
void saveStructToFile(People person[], int count, string filePath);
bool checkFileExistence(string filePath);
int getFileLineCout(string filePath);
void searchPeople(People person[], int arrSize, string searchQueryFirstName, int &index);
bool changePeople(People person[], int i);
void printStruct(People person[], int i);
void addPeople(People person[], int i);
bool delContact(People person[], int i, int lastElement);

#endif

