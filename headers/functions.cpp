#include "functions.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
#include <filesystem>
using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::getline;


int menu(void){
  printf("OPTIONS:\n1: Add contact\n2: Search\n3: Print all\n4: Print Specific\n5: Edit contact\n6: Delete contact\n7: Sort all\n8: Save Changes\n9: Exit\n");
  string  userChoiceChar;
  getline(cin, userChoiceChar);
  int userChoice = stoi(userChoiceChar);
  return userChoice;
}

//swap func for struct
void swap(People &x, People &y){ //pass struct People elements x and y by reference
  People temp = x;
  x = y;
  y = temp;
}

//bubble sort o(n^2) sorting algo but to lazy to figure other ones out especially when comparing chars
void sortArr(People unsortedArr[], int arrSize){ //pass People arr* and size of struct arr
  for (int i = 0; i < arrSize; i++){
    for (int j = 0; j < i; j++){
      if (unsortedArr[i].lastName < unsortedArr[j].lastName){
        swap(unsortedArr[i], unsortedArr[j]);
      }
    }
  }
}

//nice small little func that i saw while looking through dr. hwangs examples
void hitEnter(void){
  cout << "\nPress Enter to continue . . . ";
  string s;
  getline(cin, s);
}

//func to save struct People to file in same wd
void saveStructToFile(People person[], int count, string filePath) {
    std::ofstream file(filePath, std::ios::trunc); // Open file in truncate mode to clear existing content

    if (!file.is_open()) {
        std::cerr << "Error opening the file for writing." << endl;
        return;
    }

    for (int i = 0; i < count; i++) {
        file << person[i].firstName << " " << person[i].lastName << " ";
        file << person[i].age << " ";
        file << person[i].phoneNum << " ";
        file << person[i].email << " ";
        file << person[i].state << " ";
        file << person[i].city << " ";
        file << person[i].address << " ";
        file << person[i].zip << endl;
    }

    file.close(); // Close the file
}

//checks if file exists in current working dir
bool checkFileExistence(string filePath){
  if (std::filesystem::exists(filePath)) {
    return true;
  }
  return false;
}

//basicly simplified wc -l
int getFileLineCout(string filePath){
    std::ifstream file(filePath);
    if (!file.is_open()) {
        std::cerr << "Error opening file: " << filePath << endl;
        return -1;
    }

    int lineCount = 0;
    string line;
    while (getline(file, line)) {
        lineCount++;
    }

    file.close();
    return lineCount;
}

//terible space and time complexity but idk
void searchPeople(People person[], int arrSize, string searchQueryFirstName, int &index){
  for (int i = 0; i < arrSize; i++){
    if (strncasecmp(person[i].firstName.c_str(), searchQueryFirstName.c_str(), person[i].firstName.length()) == 0){
      index = i;
      break;
    }
    index = -1;
  }
}

//add people func
void addPeople(People person[], int i){ //pass in People struct arr and what element
    cout << endl << "First name: ";
    string addFirstName;
    getline(cin, addFirstName);
    person[i].firstName = addFirstName;
    cout  << "Last name: ";
    string addLastName;
    getline(cin, addLastName);
    person[i].lastName = addLastName;
    cout << "Age: ";
    string ageChar;
    getline(cin, ageChar);
    person[i].age = ageChar;
    cout << "Phone num: ";
    string phoneChar;
    getline(cin, phoneChar);
    person[i].phoneNum = phoneChar;
    cout << "Email: ";
    string addEmail;
    getline(cin, addEmail);
    person[i].email = addEmail;
    cout  << "State: ";
    string addState;
    getline(cin, addState);
    person[i].state = addState;
    cout  << "City: ";
    string addCity;
    getline(cin, addCity);
    person[i].city = addCity;
    cout  << "Home address: ";
    string addAddress;
    getline(cin, addAddress);
    person[i].address = addAddress;
    cout  << "Zip code: ";
    string zipChar;
    getline(cin, zipChar);
    person[i].zip = zipChar;
}

//type as bool becasue instructions said it had to be, would have made void otherwise probably (not sure if that would be worse or better?)
//basicly same as add func but changing one that exists
bool changePeople(People person[], int i){
  bool changeStatus = false;
  string changeFirstName;
  string changeLastName;
  string changeAge;
  string changePhone;
  string changeEmail;
  string changeState;
  string changeCity;
  string changeAddress;
  string changeZip;
  string userInput;
  cout << "Which element would you like to change?" << endl;
  cout << "1: First name" << endl;
  cout << "2: Last name" << endl;
  cout << "3: Age";
  cout << endl << "4: Phone num";
  cout << endl << "5: Email";
  cout << endl << "6: State";
  cout << endl << "7: City";
  cout << endl << "8: Home address";
  cout << endl << "9: Zip code";
  cout << endl << "10: Back" << endl;
  getline(cin, userInput);
  int userInputInt = stoi(userInput);
  switch(userInputInt){
    case 1:
      cout << endl << "First name: ";
      getline(cin, changeFirstName);
      person[i].firstName = changeFirstName;
      break;
    case 2:
      cout  << "Last name: ";
      getline(cin, changeLastName);
      person[i].lastName = changeLastName;
      break;
    case 3:
      cout << "Age: ";
      getline(cin, changeAge);
      person[i].age = changeAge;
      break;
    case 4:
      cout << "Phone num: ";
      getline(cin, changePhone);
      person[i].phoneNum = changePhone;
      break;
    case 5:
      cout << "Email: ";
      getline(cin, changeEmail);
      person[i].email = changeEmail;
      break;
    case 6:
      cout  << "State: ";
      getline(cin, changeState);
      person[i].state = changeState;
      break;
    case 7:
      cout  << "City: ";
      getline(cin, changeCity);
      person[i].city = changeCity;
      break;
    case 8:
      cout  << "Home address: ";
      getline(cin, changeAddress);
      person[i].address = changeAddress;
      break;
    case 9:
      cout  << "Zip code: ";
      getline(cin, changeZip);
      person[i].zip = changeZip;
      break;
    case 10:
      cout << "Returning to menu\n";
      return changeStatus;
    default:
      cout << "No correct input given...returning to menu\n";
      return changeStatus;
  }
  changeStatus = true;
  return changeStatus;
}

//just prints to stdout func
void printStruct(People person[], int i){
    cout << endl << "First name: " << person[i].firstName;
    cout << endl << "Last name: " << person[i].lastName;
    cout << endl << "Age: " << person[i].age;
    cout << endl << "Phone num: " << person[i].phoneNum;
    cout << endl << "Email: " << person[i].email;
    cout << endl << "State: " << person[i].state;
    cout << endl << "City: " << person[i].city;
    cout << endl << "Home address: " << person[i].address;
    cout << endl << "Zip code: " << person[i].zip << endl;
}


//type as bool becasue instructions said it had to be, would have made void otherwise probably (not sure if that would be worse or better?)
//swaps the last arr element with the one that will be removed then sets all members to ""
bool delContact(People person[], int i, int lastElement){
    bool delStatus = false;
    swap(person[lastElement], person[i]);
    person[lastElement].firstName = "";
    person[lastElement].lastName = "";
    person[lastElement].age = "";
    person[lastElement].phoneNum = "";
    person[lastElement].email = "";
    person[lastElement].state = "";
    person[lastElement].city = "";
    person[lastElement].address = "";
    person[lastElement].zip = "";
    delStatus = true;
    if (delStatus == true){
      cout << endl << "Successfuly deleted contact" << endl;
    }
    return delStatus;
}
